package org.opencv.samples.MusicRec;


import android.media.MediaPlayer;
import android.util.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * This class is used to play sounds founds in the processed image
 */

public class SoundPlayer {

    int i = 0;
    ArrayList<Note> noteToPlay = new ArrayList<Note>();
    String path;
    String TAG = "SoundPlayer";

    public SoundPlayer(String directorySoundPath)
    {
        path = directorySoundPath;

    }

    public void audioPlayer(String fileName){


        //set up MediaPlayer
        final MediaPlayer mp = new MediaPlayer();
        i++;



        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {

                playNextSound();
                mp.release();
            }
        });

        try {
            mp.setDataSource(path + fileName);
            mp.prepare();
            mp.start();

        }

        catch (Exception e) {

            //e.printStackTrace();
        }

    }

    public void playNextSound()
    {

        if (i<noteToPlay.size())
        {
            audioPlayer(noteToPlay.get(i).getHeight() + ".mp3");
        }
    }


    public void playNoteDetected(ArrayList<Note> noteDetected, int rowDetected) {

        ArrayList<ArrayList<Note>> n = new ArrayList<ArrayList<Note>>();

        for(int i = 0 ; i<rowDetected; i++)
        {
            ArrayList<Note> temp = new ArrayList<Note>();
            n.add(temp);
        }

        //Insert noteDetected into correct list
        for (int k = 0 ; k < noteDetected.size();k++)
        {
            ArrayList<Note> temp = new ArrayList<Note>();
            int index = noteDetected.get(k).getNumRow();
            temp = n.get(index);
            temp.add(noteDetected.get(k));
            n.remove(index);
            n.add(index,temp);
        }

        //Order list of noteDetected by rows, and add in list that will be sound

        for (int i = 0 ; i<n.size();i++)
        {
            Collections.sort (n.get(i), new  Comparator<Note>() {
                @Override
                public int compare(Note note, Note note2) {
                    return note.compareTo(note2);
                }
            });

            noteToPlay.addAll(n.get(i));
        }

        playNextSound();


    }
}
