package org.opencv.samples.MusicRec;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.Toast;

import com.example.luigi.trainingopencv.R;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;

/**
 * This class implements the activity that manages elaboration of image
 */


public class ElaborationActivity extends Activity {

    final String TAG = "Elaboration-Activity";
    String mainDirPath;
    String filePath;

    ArrayList<Mat> imageToView= new ArrayList<Mat>();
    ArrayList<Rect> halfnotes = new ArrayList<Rect>();
    ArrayList<Rect> trebleclef = new ArrayList<Rect>();
    ArrayList<Rect> sharps = new ArrayList<Rect>();
    ArrayList<Rect> object = new ArrayList<Rect>();
    ArrayList<Note> noteDetected = new ArrayList<Note>();

    Mat src;
    Mat bw_img ;
    Mat detectedstave_img ;
    Mat nostave_img ;
    Mat notedetected_img ;

    int currentRoi = 0;
    int param = 1;
    double param1 = 0;
    int param2, param3 = 0;

    private Button buttonNext = null;
    private Button buttonPrev = null;
    private Button buttonElab = null;
    private Button buttonPlay = null;

    private ProgressBar progress = null;

    private SeekBar showStepSeekBar = null;
    private SeekBar param1SeekBar = null;
    private SeekBar param2SeekBar = null;
    private SeekBar param3SeekBar = null;

    Sheet stave = new Sheet();
    private SoundPlayer sp;
    private LinearLayout advancedOption = null;
    private boolean optionIsVisible = false;
    private String selectedInstruments = "Piano";


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_elaboration_support);
        progress = (ProgressBar)findViewById(R.id.progressBar);
        progress.setVisibility(View.VISIBLE);
        processImage();
        advancedOption = (LinearLayout)findViewById(R.id.advanced_option_panel);
        advancedOption.setVisibility(View.GONE);


    }
    /**
     * The method processImage starts the threads that process the image.
     *
     *
     *
     */
    private void processImage() {
        Intent intent = getIntent();
        filePath = intent.getStringExtra("photoPath");
        mainDirPath = intent.getStringExtra("mainDirPath");
        ProcessTask processTemplate = new ProcessTask(new AsyncResponse() {
            @Override
            public void processFinish(ArrayList<ArrayList<Rect>> output) {
                halfnotes = output.get(0);
                trebleclef = output.get(1);
                sharps = output.get(2);
            }
            @Override
            public void processFinish(ProcessingResult output) {
             // empty
            }
        });

        processTemplate.setParam1(param1);
        processTemplate.execute(filePath);

        ImageElaborationTask imageElaboration = new ImageElaborationTask(new AsyncResponse() {
            @Override
            public void processFinish(ArrayList<ArrayList<Rect>> output) {
            //empty
            }

            @Override
            public void processFinish(ProcessingResult results) {

                src = results.mats.get(0);
                bw_img = results.mats.get(1);
                detectedstave_img= results.mats.get(2);
                nostave_img= results.mats.get(3);
                notedetected_img = results.mats.get(4);
                stave = results.stave;
                noteDetected = results.noteDetected;
                object = results.object;

                imageToView.add(src);
                imageToView.add(bw_img);
                imageToView.add(detectedstave_img);
                imageToView.add(nostave_img);
                imageToView.add(notedetected_img);
                imageToView.add(computeImageToView(src));

                DisplayImage(imageToView.get(5));
                progress.setVisibility(View.GONE);
                initGui(notedetected_img);

            }
        });
        imageElaboration.setParam2(param2);
        imageElaboration.setParam3(param3);
        imageElaboration.execute(filePath);
    }

    /**
     * This function creates final image, that contains bounding boxes for every musical objects founds
     *@param src The source image
     *@return Source image with bounding boxes
     */
    private Mat computeImageToView(Mat src){
       Mat finalImage = new Mat();
        src.copyTo(finalImage);
        Imgproc.cvtColor(finalImage,finalImage,Imgproc.COLOR_GRAY2BGR);

        //Add noteDetected detected
        for (int i = 0 ; i< noteDetected.size();i++) {
            Rect boundingBox = noteDetected.get(i).getBoundingboxNote();
            Core.rectangle(finalImage, new Point(boundingBox.x, boundingBox.y), new Point(boundingBox.x + boundingBox.width, boundingBox.y + boundingBox.height),Color.COLOR_RED, 10);
        }

        //Add stave detected
        for (int i = 0; i<stave.musicalrow.size();i++)
        {
            Point center = new Point(finalImage.cols()/2,stave.musicalrow.get(i).get(5));
            Core.circle(finalImage, center , 20, Color.COLOR_GREEN, -1,8,0);
            center = new Point(finalImage.cols()/2,stave.musicalrow.get(i).get(7));
            Core.circle(finalImage, center , 20, Color.COLOR_GREEN, -1,8,0);
            center = new Point(finalImage.cols()/2,stave.musicalrow.get(i).get(9));
            Core.circle(finalImage, center , 20, Color.COLOR_GREEN, -1,8,0);
            center = new Point(finalImage.cols()/2,stave.musicalrow.get(i).get(11));
            Core.circle(finalImage, center , 20, Color.COLOR_GREEN, -1,8,0);
            center = new Point(finalImage.cols()/2,stave.musicalrow.get(i).get(13));
            Core.circle(finalImage, center , 20, Color.COLOR_GREEN, -1,8,0);
        }

        // Add halfnote
        for(Rect boundingBox :halfnotes)
        {
            Core.rectangle(finalImage, new Point(boundingBox.x, boundingBox.y), new Point(boundingBox.x + boundingBox.width, boundingBox.y + boundingBox.height),Color.COLOR_BLUE, 10);
        }

        for(Rect boundingBox :sharps)
        {
            Core.rectangle(finalImage, new Point(boundingBox.x, boundingBox.y), new Point(boundingBox.x + boundingBox.width,boundingBox.y + boundingBox.height),Color.COLOR_VIOLET, 20);
        }

        for(Rect boundingBox :trebleclef)
        {
            Core.rectangle(finalImage, new Point(boundingBox.x, boundingBox.y), new Point(boundingBox.x + boundingBox.width, boundingBox.y + boundingBox.height),Color.COLOR_YELLOW, 30);
        }

        return finalImage;

    }


    private void initGui(final Mat imgToSlide) {
        buttonNext = (Button)findViewById(R.id.buttonNext);
        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showRoi(imgToSlide);
                updateCurrentRoi(+1);
            }
        });

        buttonPrev = (Button)findViewById(R.id.buttonPrev);
        buttonPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showRoi(imgToSlide);
                updateCurrentRoi(-1);
            }
        });

        buttonElab = (Button)findViewById(R.id.buttonDoRoiElab);
        buttonElab.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                progress.setVisibility(View.VISIBLE);
                buttonElab.setClickable(false);
                buttonElab.setEnabled(false);
                advancedOption.setVisibility(view.GONE);
                optionIsVisible = false;
                ProcessTask processTemplate = new ProcessTask(new AsyncResponse() {
                    @Override
                    public void processFinish(ArrayList<ArrayList<Rect>> output) {
                        halfnotes = output.get(0);
                        trebleclef = output.get(1);
                        sharps = output.get(2);
                        buttonElab.setClickable(true);
                        buttonElab.setEnabled(true);
                        //progress.setVisibility(View.GONE);
                    }
                    @Override
                    public void processFinish(ProcessingResult output) {
                        // empty
                    }
                });
                processTemplate.setParam1(param1);
                processTemplate.execute(filePath);

                ImageElaborationTask imageElaboration = new ImageElaborationTask(new AsyncResponse() {
                    @Override
                    public void processFinish(ArrayList<ArrayList<Rect>> output) {
                        //empty
                    }

                    @Override
                    public void processFinish(ProcessingResult results) {

                        src = results.mats.get(0);
                        bw_img = results.mats.get(1);
                        detectedstave_img= results.mats.get(2);
                        nostave_img= results.mats.get(3);
                        notedetected_img = results.mats.get(4);

                        stave = results.stave;
                        noteDetected = results.noteDetected;
                        object = results.object;

                        imageToView.add(src);
                        imageToView.add(bw_img);
                        imageToView.add(detectedstave_img);
                        imageToView.add(nostave_img);
                        imageToView.add(notedetected_img);
                        imageToView.add(computeImageToView(src));

                        DisplayImage(imageToView.get(5));
                        progress.setVisibility(View.GONE);
                        initGui(notedetected_img);
                        buttonElab.setClickable(true);
                        buttonElab.setEnabled(true);

                    }
                });
                imageElaboration.setParam2(param2);
                imageElaboration.setParam3(param3);
                imageElaboration.execute(filePath);
            }
        });

        buttonPlay = (Button)findViewById(R.id.buttonPlaySound);
        buttonPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Play sound

                buttonPlay.setEnabled(false);
                buttonPlay.setClickable(false);
                sp = new SoundPlayer(mainDirPath+"/Sounds/"+selectedInstruments+"/");
                sp.playNoteDetected(noteDetected,stave.musicalrow.size());
                buttonPlay.setEnabled(true);
                buttonPlay.setClickable(true);

            }
        });

        showStepSeekBar = (SeekBar)findViewById(R.id.seekBar3);
        showStepSeekBar.setMax(5);
        showStepSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progresValue, boolean fromUser) {
                param = progresValue;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

                Toast.makeText(getApplicationContext(), "STEP " + new Integer(param).toString(), Toast.LENGTH_SHORT).show();
                if (param == 5) {
                    imageToView.remove(5);
                    imageToView.add(5,computeImageToView(src));
                }
                DisplayImage(imageToView.get(param));
            }
        });

        param1SeekBar = (SeekBar)findViewById(R.id.param1SeekBar);
        param1SeekBar.setMax(20);
        param1SeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progresValue, boolean fromUser) {

                param1 = (progresValue-10);

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        param2SeekBar = (SeekBar)findViewById(R.id.param2SeekBar);
        param2SeekBar.setMax(20);
        param2SeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progresValue, boolean fromUser) {
                param2 = progresValue-10;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        param3SeekBar = (SeekBar)findViewById(R.id.param3SeekBar);
        param3SeekBar.setMax(20);
        param3SeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progresValue, boolean fromUser) {
                param3 = progresValue-10;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    /**
     * This function update the current image to view
     *@param i current value of index
     *
     */
    private void updateCurrentRoi(int i) {

        if (i>0)
        {
            if(currentRoi + i >= object.size())
            {
                currentRoi=0;
            }
            else
            {
                currentRoi+=i;
            }
        }
        else
        {
            if (currentRoi + i < 0)
            {
                currentRoi = object.size()-1;
            }
            else
            {
                currentRoi+=i;
            }
        }
    }

    /**
     * This function set the image to display on screen
     *@param toShow The image to show on display
     *
     */
    private void DisplayImage(Mat toShow){
        Bitmap bm = Bitmap.createBitmap(toShow.cols(), toShow.rows(),Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(toShow, bm);
        ImageView iv = (ImageView) findViewById(R.id.imageView2);
        iv.setImageBitmap(bm);
    }

    /**
     * This function set the ROI to display on screen
     *@param imgToSlide The ROI to show on display
     *
     */
    private void showRoi (Mat imgToSlide){
        Rect roi = object.get(currentRoi);
        Mat imgROI = new Mat(imgToSlide,roi);
        DisplayImage(imgROI);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id=item.getItemId();
        switch(id)
        {
            // impostazioni avanzate
            case R.id.advanced_settings:

                if(optionIsVisible)
                {
                    advancedOption.setVisibility(View.GONE);
                    optionIsVisible = false;
                }
                else
                {
                    advancedOption.setVisibility(View.VISIBLE);
                    optionIsVisible=true;

                }
                break;

            // seleziona strumento
            case R.id.choose_player:

                showAlertDialog();
                break;
            // credits
            case R.id.credit:
                showCredits();
                break;

        }
        return false;
    }

    private void showAlertDialog()
    {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(this);
        builderSingle.setIcon(R.drawable.ic_launcher);
        builderSingle.setTitle("Seleziona strumento");
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,android.R.layout.select_dialog_item);

        // add here all instruments sounds available
        arrayAdapter.add("Piano");
        arrayAdapter.add("Flute");

        builderSingle.setAdapter(arrayAdapter,
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        selectedInstruments = arrayAdapter.getItem(which);
                        dialog.dismiss();
                    }
                });

        builderSingle.setNegativeButton("Indietro",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });


    builderSingle.show();
    }


    private void showCredits(){
        AlertDialog.Builder builderInner = new AlertDialog.Builder(ElaborationActivity.this);
        builderInner.setMessage("Applicazione realizzata da\nAlberto Cannavò\nLuigi Celona");
        builderInner.setTitle("Credits");
        builderInner.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick( DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        builderInner.show();
    }

}

