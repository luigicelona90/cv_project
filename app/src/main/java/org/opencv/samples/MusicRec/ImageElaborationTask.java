package org.opencv.samples.MusicRec;

import android.os.AsyncTask;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;

public class ImageElaborationTask extends AsyncTask<String, Void , Void> {
    public AsyncResponse delegate = null;
    Mat src;
    Mat bw_img ;
    Mat detectedstave_img ;
    Mat nostave_img ;
    Mat objdetected_img ;
    Mat notedetected_img ;
    ArrayList<Rect> object = new ArrayList<Rect>();
    ArrayList<Note> noteDetected = new ArrayList<Note>();
    Sheet stave =  new Sheet();

    private int param2, param3;

    public void setParam2(int value)
    {
        this.param2= value;
    }
    public void setParam3(int value)
    {
        this.param3= value;
    }


    public ImageElaborationTask(AsyncResponse asyncResponse){
        delegate = asyncResponse;
    }

    @Override
    protected Void doInBackground(String... paths) {
        src = Highgui.imread(paths[0]);
        bw_img = preProcessing(src);
        detectedstave_img = detectStaves(bw_img);
        nostave_img = deleteStave(bw_img);
        objdetected_img = detectObject(nostave_img);
        notedetected_img = doRoiElaboration(nostave_img);
        return null;
    }

    @Override
    protected  void onPostExecute(Void v){
        // bar.setVisibility(View.GONE);
        ProcessingResult results = new ProcessingResult();
        results.mats.add(src);
        results.mats.add(bw_img);
        results.mats.add(detectedstave_img);
        results.mats.add(nostave_img);
        results.mats.add(notedetected_img);
        results.stave = stave;
        results.noteDetected = noteDetected;
        results.object = object;
        delegate.processFinish(results);
    }

    /**
     * This function apply the preprocessing operation(smoothing, color change, threshold) in the source image
     *@param image
     *
     */
    private Mat preProcessing(Mat image){
        //blur to image
        Imgproc.blur(image, image, new Size(10, 10));
        //convert to gray scale
        Imgproc.cvtColor(image, image, Imgproc.COLOR_BGR2GRAY);
        Mat bw = new Mat();

        Imgproc.threshold(image,bw,125,255,1);
        return bw;
    }
    /**
     * This function has the aim to find pentagram position in the image. This is important to detect after notes height
     *@param bw_img Image in black and white form
     *@return Image with detected staves
     */
    private Mat detectStaves(Mat bw_img){

        Mat horizontal = bw_img.clone();
        // Specify size on horizontal axis
        int horizontalsize = horizontal.cols() / (30 + param2);
        // Create structure element for extracting horizontal lines through morphology operations
        Mat horizontalStructure = Imgproc.getStructuringElement( Imgproc.CV_SHAPE_RECT, new Size(horizontalsize,1));
        // Apply OPENING operation
        Imgproc.erode(horizontal, horizontal, horizontalStructure, new Point(-1, -1),1);
        Imgproc.dilate(horizontal, horizontal, horizontalStructure, new Point(-1, -1),1);

        Mat horizontal2 = horizontal.clone();

        Mat vertical = bw_img.clone();
        int verticalsize = vertical.rows() / (70 + param2*2);
        Mat verticalStructure = Imgproc.getStructuringElement( Imgproc.CV_SHAPE_RECT, new Size(1, verticalsize));
        Imgproc.erode( horizontal2, horizontal2, verticalStructure, new Point(-1, -1),1);
        Imgproc.dilate( horizontal2, horizontal2,verticalStructure, new Point(-1, -1),1);

        Core.subtract(horizontal, horizontal2, horizontal);

        Mat structelem = Imgproc.getStructuringElement( Imgproc.CV_SHAPE_RECT, new Size(horizontal.cols()/300, horizontal.rows()/300));
        Mat horizontalStructure2 = Imgproc.getStructuringElement( Imgproc.CV_SHAPE_RECT, new Size(horizontal.cols()/4,1));
        Imgproc.erode( horizontal, horizontal, structelem, new Point(-1, -1),1);
        Imgproc.dilate(horizontal, horizontal, horizontalStructure2, new Point(-1, -1),1);

        Mat drawing = horizontal.clone();
        ArrayList<MatOfPoint> contours  = new ArrayList<MatOfPoint>();
        Mat hierarchy = new Mat();
        Imgproc.findContours(drawing, contours, hierarchy, Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE, new Point(0,0));
        Imgproc.cvtColor(horizontal, horizontal, Imgproc.COLOR_GRAY2BGR);
        for( int i = 0; i < contours.size(); i++ ) {
            MatOfPoint2f curve = new MatOfPoint2f();
            MatOfPoint2f approxcurve = new MatOfPoint2f();
            contours.get(i).convertTo(curve, CvType.CV_32FC2);
            //approximates a polygonal curve
            Imgproc.approxPolyDP(curve, approxcurve, 3, true);
            MatOfPoint points = new MatOfPoint(approxcurve.toArray());
            Rect rect = Imgproc.boundingRect(points);
            if(rect.width >= horizontal.width()*5/6){
                Core.circle(horizontal, new Point(rect.x + rect.width/2,rect.y + rect.height/2 ) , 20, new Scalar(0,255,0,255), -1,8,0);
                stave.addStave(rect.y + rect.height/2);
            }
        }
        
        stave.makeStave();
        return horizontal;

    }

    /**
     * This function delete the musical staves from the image black-and-white. The purpose of this operation is to make easier the object recognition
     *@param bw_img Image in black and white form
     *@return Image without musical staves
     */
    private Mat deleteStave(Mat bw_img){
        Mat vertical = bw_img.clone();
        // Specify size on vertical axis
        int verticalsize = vertical.rows() / 60;
        // Create structure element for extracting vertical lines through morphology operations
        Mat verticalStructure = Imgproc.getStructuringElement( Imgproc.CV_SHAPE_RECT, new Size(1, verticalsize));
        // Apply OPENING operation
        Imgproc.erode( vertical, vertical, verticalStructure, new Point(-1, -1),1);
        Imgproc.dilate( vertical, vertical,verticalStructure, new Point(-1, -1),1);
        // Inverse vertical image
        Core.bitwise_not(vertical, vertical);
        //    DisplayImage(bw_img);
        return vertical;

    }
    /**
     * This function is used to detect musical note in the image. In this function all the objects detected are saved in an array called "object"
     *@param nostave_img Image without staves
     *@return Image without staves, after the application of findContours method
     */
    private Mat detectObject(Mat nostave_img){
        Mat objdetected_img = new Mat();
        Mat drawing = new Mat();
        nostave_img.copyTo(drawing);
        nostave_img.copyTo(objdetected_img);

        ArrayList<MatOfPoint> contours  = new ArrayList<MatOfPoint>();
        Mat hierarchy = new Mat();
        //find objects image
        Imgproc.findContours(drawing, contours, hierarchy, Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE, new Point(0,0));

        //find all the object and define bounding box(rect)
        for( int i = 0; i < contours.size(); i++ ) {
            MatOfPoint2f curve = new MatOfPoint2f();
            MatOfPoint2f approxcurve = new MatOfPoint2f();
            contours.get(i).convertTo(curve, CvType.CV_32FC2);
            //approximates a polygonal curve
            Imgproc.approxPolyDP(curve, approxcurve, 3, true);
            MatOfPoint points = new MatOfPoint(approxcurve.toArray());
            Rect rect = Imgproc.boundingRect(points);
            object.add(rect);
        }
        return objdetected_img;
    }
    /**
     * This function process all the objects founds to find musical notes and their height
     *@param nostave_img Image without staves
     *@return Image without staves and with bounding boxes on all the musical notes found
     */
    private Mat doRoiElaboration(Mat nostave_img) {
        Mat notedetected_img = nostave_img.clone();
        Imgproc.cvtColor(notedetected_img, notedetected_img, Imgproc.COLOR_GRAY2BGR);
        //For all the object found in the image, check if there are musical notes
        for(int current = 0; current < object.size(); current++) {
            //Define ROI
            Rect rect = object.get(current);
            if(rect.width > nostave_img.width() - 10 || rect.height > nostave_img.height()-10){
                continue;
            }
            // Get ROI to image
            Mat imgROI = new Mat(nostave_img, rect);
            Mat imgRoiElab = new Mat();
            Mat drawing = new Mat();
            imgROI.copyTo(imgRoiElab);

            // Elaboration
            Core.bitwise_not(imgRoiElab, imgRoiElab);
            Mat structElem = Imgproc.getStructuringElement(Imgproc.CV_SHAPE_RECT, new Size(30+param3*2, 16+param3));
            Imgproc.erode(imgRoiElab, imgRoiElab, structElem, new Point(-1, -1), 2);
            Core.bitwise_not(imgRoiElab, imgRoiElab);

            // Init for findContours
            imgRoiElab.copyTo(drawing);
            ArrayList<MatOfPoint> contours = new ArrayList<MatOfPoint>();
            Mat hierarchy = new Mat();
            Imgproc.findContours(drawing, contours, hierarchy, Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE, new Point(0, 0));
            //detect height of notes
            for (int i = 0; i < contours.size(); i++) {
                noteHeightDetection(contours, rect, notedetected_img,i);
            }
        }
        return notedetected_img;
    }

    /**
     * This function detect the height of musical notes, and add the bounding boxes to the image without staves
     *@param contours Arraylist that contains information about contours of the object detected
     *@param rect ROI of source image that contains the object
     *@param  notedetected_img Image in which write bounding boxes
     *@param currentContours Index of the objects list, indicate the current object
     */
    private void noteHeightDetection(ArrayList<MatOfPoint> contours, Rect rect, Mat notedetected_img, int currentContours) {

        //Compute object bounding box
        int contourSize = contours.size();
        MatOfPoint2f curve = new MatOfPoint2f();
        MatOfPoint2f approxcurve = new MatOfPoint2f();
        contours.get(currentContours).convertTo(curve, CvType.CV_32FC2);
        Imgproc.approxPolyDP(curve, approxcurve, 3, true);
        MatOfPoint points = new MatOfPoint(approxcurve.toArray());
        Rect box = Imgproc.boundingRect(points);

        if (!(rect.width - 5 < box.width && rect.height - 5 < box.height))
        {

            if (contourSize >= 2 && contourSize < 20) {
                //pallino nota singola
                if (box.height >= box.width - 5 && box.height <= 3 * box.width) {

                    Note n = new Note();
                    n.setBoundingboxNote(rect);
                    n.setCenter(new Point(rect.x + box.x + box.width / 2, rect.y + box.y + box.width / 2));
                    String height = stave.getNoteHeight(rect.y + box.y + box.width / 2);
                    if (!height.equals(stave.err)) {
                        n.setHeight(height);
                        n.setNumRow(stave.getNumRow(rect.y + box.y + box.width / 2));
                        noteDetected.add(n);
                        Core.rectangle(notedetected_img, new Point(rect.x + box.x, rect.y + box.y), new Point(rect.x + box.x + box.width, rect.y + box.y + box.height), new Scalar(255, 0, 0, 255), 10);
                        Core.putText(notedetected_img, height, new Point(rect.x + box.x + box.width / 2, rect.y + box.y + 3*box.height/2  ), Core.FONT_HERSHEY_TRIPLEX,1.5, Color.COLOR_WHITE);

                    }

                }
            }
        }

    }


}
