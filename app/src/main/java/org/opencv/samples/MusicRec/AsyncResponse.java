package org.opencv.samples.MusicRec;

import org.opencv.core.Rect;

import java.util.ArrayList;

/**
 * This is an interface that is used to transport results from the asyntask to ElaborationActivity
 */

//
public interface AsyncResponse {

    void processFinish(ArrayList<ArrayList<Rect>> objectDetected);

    void processFinish(ProcessingResult results);

}
