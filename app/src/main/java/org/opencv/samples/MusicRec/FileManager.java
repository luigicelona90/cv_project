package org.opencv.samples.MusicRec;


import android.os.Environment;
import java.io.File;
import java.util.ArrayList;


/**
 * This class allows to manage the list of images
 *
 *
 */

public class FileManager {

    public static ArrayList<File> getListFiles() {
        File parentDir = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+ "/MusicRec");
        ArrayList<File> inFiles = new ArrayList<File>();
        File[] files = parentDir.listFiles();
        for (File file : files) {
            if(file.getName().endsWith(".jpg")){
                inFiles.add(file);
            }
        }
        return inFiles;
    }

}
