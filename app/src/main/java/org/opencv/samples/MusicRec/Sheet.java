package org.opencv.samples.MusicRec;

import android.util.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.NavigableMap;
import java.util.TreeMap;

/**
 * Created by Luigi on 19/03/2015.
 */

/**
 * This class contain the information about the musical sheet. In is used to detect height of musical notes
 */

public class Sheet {
    public final String err = "error";
    ArrayList<Integer> staves ;
    ArrayList<ArrayList<Integer>> musicalrow;

    NavigableMap<Integer, String> map;
    NavigableMap<Integer, Integer> rowMap;

    /**
     * This function is used to initialize map of musical heights
     */
    private void initializeMap(){
        map = new TreeMap<Integer, String>();
        rowMap = new TreeMap<Integer, Integer>();

        for(int i = 0; i< musicalrow.size(); i++){

            int rowCenter = (musicalrow.get(i).get(musicalrow.get(i).size() - 1) + musicalrow.get(i).get(0))/2;
            rowMap.put(rowCenter,i);

            for(int j = 0; j < musicalrow.get(i).size(); j++){

                switch (j){
                    case 0 :
                        map.put(musicalrow.get(i).get(j), "RE5");
                        break;
                    case 7 :
                        map.put(musicalrow.get(i).get(j), "RE4");
                        break;
                    case 14 :
                        map.put(musicalrow.get(i).get(j), "RE3");
                        break;

                    case 1 :
                        map.put(musicalrow.get(i).get(j), "DO5");
                        break;
                    case 8 :
                        map.put(musicalrow.get(i).get(j), "DO4");
                        break;
                    case 15 :
                        map.put(musicalrow.get(i).get(j), "DO3");
                        break;

                    case 2 :
                        map.put(musicalrow.get(i).get(j), "SI4");
                        break;
                    case 9 :
                        map.put(musicalrow.get(i).get(j), "SI3");
                        break;
                    case 16 :
                        map.put(musicalrow.get(i).get(j), "SI2");
                        break;

                    case 3 :
                        map.put(musicalrow.get(i).get(j), "LA4");
                        break;
                    case 10 :
                        map.put(musicalrow.get(i).get(j), "LA3");
                        break;
                    case 17 :
                        map.put(musicalrow.get(i).get(j), "LA2");
                        break;

                    case 4 :
                        map.put(musicalrow.get(i).get(j), "SOL4");
                        break;

                    case 11 :
                        map.put(musicalrow.get(i).get(j), "SOL3");
                        break;
                    case 18:
                        map.put(musicalrow.get(i).get(j), "SOL2");
                        break;

                    case 5 :
                        map.put(musicalrow.get(i).get(j), "FA4");
                        break;
                    case 12 :
                        map.put(musicalrow.get(i).get(j), "FA3");
                        break;

                    case 6 :
                        map.put(musicalrow.get(i).get(j), "MI4");
                        break;
                    case 13 :
                        map.put(musicalrow.get(i).get(j), "MI3");
                        break;
                }
            }
        }
    };

    /**
     * This function return the height of musical notes, starting from their y-axis position
     * @param note_y y-axis value of musical note
     * @return represents the height of note
     */
    public String getNoteHeight(int note_y){
       String height;
        try {
           if (note_y - map.floorKey(note_y) > map.ceilingKey(note_y) - note_y)
           {
              height = map.get(map.ceilingKey(note_y));

           }
           else
           {
               height = map.get(map.floorKey(note_y));
           }
       }

       catch (Exception ex){
           return err;
       }
        return height;
    }
    /**
     * This function find the nearest staves position of musical sheets
     */
    public int getNumRow(int notePosition) {

        try {
            if (rowMap.ceilingKey(notePosition) == null) {
                return rowMap.get(rowMap.floorKey(notePosition));
            } else {
                if (rowMap.floorKey(notePosition) == null) {
                    return rowMap.get(rowMap.ceilingKey(notePosition));
                } else {
                    int keyFloor = rowMap.floorKey(notePosition);
                    int keyCeiling = rowMap.ceilingKey(notePosition);

                    if (notePosition - rowMap.floorKey(keyFloor) > rowMap.ceilingKey(keyCeiling) - notePosition)
                    {
                        return rowMap.get(keyCeiling);
                    }
                    else
                    {
                        return rowMap.get(keyFloor);
                    }
                }
            }
        }
        catch (Exception ex){
            Log.w("EXCEPTION",ex);
            return 0;
        }
    }

    /**
     * This function is used to create a structure that represent all the possible musical height in the current image
     */
    public void makeStave(){
        int first, second;
        int deltacurr = 0;
        int deltaold= 0;
        boolean error = false;
        if(staves.isEmpty()){
            return;
        }
        Collections.sort(staves);
        while(!staves.isEmpty() && staves.size()-5 >= 0){
            ArrayList<Integer> temp = new ArrayList<Integer>();
            first = staves.get(0);
            second = staves.get(1);
            deltacurr = second-first;

            //add virtual up row
            temp.add(first-5*deltacurr/2);
            temp.add(first-2*deltacurr);
            temp.add(first-3*deltacurr/2);
            temp.add(first-deltacurr);
            temp.add(first-deltacurr/2);

            for(int i =0; i<4;i++){
                try {
                    deltaold = deltacurr;
                    first = staves.get(0);
                    second = staves.get(1);
                    deltacurr = second-first;
                    if(Math.abs(deltacurr-deltaold) > 10 ){
                        error = true;
                        break;
                    }
                    temp.add(first);
                    temp.add(first+deltacurr/2);
                    staves.remove(0);
                }
                catch(Exception ex){
                    //rigo mal formato
                    return;
                }
            }
            if(error){
                error = false;
                continue;
            }
            first = staves.get(0);
            temp.add(first);
            temp.add(first+deltacurr/2);
            //add virtual down row
            temp.add(first+deltacurr);
            temp.add(first+3*deltacurr/2);
            temp.add(first+2*deltacurr);
            temp.add(first+5*deltacurr/2);
            staves.remove(0);
            musicalrow.add(temp);

        }
        this.initializeMap();
    }

    public int getStave(int i){
        return staves.get(i);
    }

    public void addStave( int s){
        staves.add(s);
    }

    public Sheet(){
       staves = new ArrayList<Integer>();
       musicalrow = new ArrayList<ArrayList<Integer>>();
    }
}
