package org.opencv.samples.MusicRec;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;

import com.example.luigi.trainingopencv.R;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;


public class MainActivity extends Activity {

    static final int REQUEST_TAKE_PHOTO = 1;
    final String TAG = "MainActivity";
    String mCurrentPhotoPath;
    private File storageDir = null;

    private ImageView imageView = null ;
    private Button buttonPhoto = null;
    private Button buttonStart = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout);
        initButtons();
        initMainDirectory();
        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_9, this, mLoaderCallback);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        //OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_9, this, mLoaderCallback);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK) {
            enableElaboration();
        }
        else if(requestCode == REQUEST_TAKE_PHOTO && resultCode != RESULT_OK){
            File f = new File(mCurrentPhotoPath);
            f.delete();
        }
    }
    /**
     * This function initialize all the button of starting layout
     */
    private void initButtons() {

        buttonPhoto = (Button)findViewById(R.id.buttonPhoto);
        imageView = (ImageView)findViewById(R.id.imageView);

        buttonStart = (Button)findViewById(R.id.buttonStart);
        buttonPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAlertDialog();
            }
        });

        buttonStart.setClickable(false);
        buttonStart.setEnabled(false);
        buttonStart.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent elaborationIntent = new Intent(MainActivity.this, ElaborationActivity.class);
                elaborationIntent.putExtra("photoPath", mCurrentPhotoPath); //Optional parameters
                elaborationIntent.putExtra("mainDirPath",Environment.getExternalStorageDirectory().getAbsolutePath()+ "/MusicRec");
                MainActivity.this.startActivity(elaborationIntent);
            }
        });
    }
    /**
     * This function get the name of main directory of the application. In this directory will be saved all the image of the app
     */
    private void initMainDirectory() {
        String pathDir = "/" + getString(R.string.app_name) + "/";
        storageDir = Environment.getExternalStoragePublicDirectory( pathDir );
        if(!storageDir.exists()){
            storageDir.mkdirs();
        }
    }
    /**
     * This function create the file that will contain the image
     * @return  File used to save the image
     */
    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyy-MM-dd_HHmmss").format(new Date());
        String imageFileName =  timeStamp + "_";
        File image = File.createTempFile( imageFileName,  ".jpg", storageDir );
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void takePhoto() {
        Intent takePhotoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePhotoIntent.resolveActivity(getPackageManager()) != null) {
            File photoFile = null;
            try{
                photoFile = createImageFile();
            }
            catch (IOException ex) {
            }
            if (photoFile != null) {
                takePhotoIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                startActivityForResult(takePhotoIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {

        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                {
                    Log.i(TAG, "OpenCV loaded successfully");
                } break;
                default:
                {
                    super.onManagerConnected(status);
                } break;
            }
        }
    };

    private void showAlertDialog()
    {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(this);
        builderSingle.setIcon(R.drawable.ic_launcher);
        builderSingle.setTitle("Seleziona immagine");
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,android.R.layout.select_dialog_singlechoice);
        ArrayList<File> fileList = FileManager.getListFiles();
        if(!fileList.isEmpty()) {
            for (int j = 0; j < fileList.size(); j++) {
                arrayAdapter.add(fileList.get(j).getName());
            }
            builderSingle.setAdapter(arrayAdapter,
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            String strName = arrayAdapter.getItem(which);
                            AlertDialog.Builder builderInner = new AlertDialog.Builder(MainActivity.this);
                            builderInner.setMessage(strName);
                            builderInner.setTitle("Hai selezionato: ");

                            mCurrentPhotoPath = Environment.getExternalStorageDirectory().getAbsolutePath()+ "/MusicRec/"+ strName;

                            builderInner.setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {

                                        @Override
                                        public void onClick( DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                            enableElaboration();
                                        }
                                    });
                            builderInner.show();
                        }
                    });
        }
        builderSingle.setNegativeButton("Indietro",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        builderSingle.setPositiveButton("Scatta foto",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        takePhoto();
                    }
                });
        builderSingle.show();
    }

    /**
     * This function closes MainActivity and starts ElaborationActivity,
     */
    private void enableElaboration() {
        File imagef = new File(mCurrentPhotoPath);
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        Log.d(TAG, imagef.getAbsolutePath());
        Bitmap bitmap = BitmapFactory.decodeFile(imagef.getAbsolutePath() , options);
        imageView.setImageBitmap(bitmap);
        buttonStart.setClickable(true);
        buttonStart.setEnabled(true);
    }

}