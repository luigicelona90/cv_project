package org.opencv.samples.MusicRec;

import android.os.AsyncTask;


import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;

/**
 *    This is the class that implements object recognition using template
 */
public class ProcessTask extends AsyncTask<String, Void, Void> {

    public AsyncResponse delegate = null;

    ArrayList<Rect> halfnotes = new ArrayList<Rect>();
    ArrayList<Rect> trebleclef = new ArrayList<Rect>();
    ArrayList<Rect> sharps = new ArrayList<Rect>();

    double param1;

    public ProcessTask(AsyncResponse asyncResponse){
        delegate = asyncResponse;
    }

    public void setParam1(double value)
    {
        this.param1= value;
    }



    @Override
    protected Void doInBackground(String... paths) {
        Mat src = Highgui.imread(paths[0]);
        processTemplate(src);
        return null;
    }

    @Override
    protected  void onPostExecute(Void v){
       // bar.setVisibility(View.GONE);
        ArrayList<ArrayList<Rect>> objdetected = new ArrayList<ArrayList<Rect>>();
        objdetected.add(halfnotes);
        objdetected.add(trebleclef);
        objdetected.add(sharps);
        delegate.processFinish(objdetected);
    }



    private void processTemplate(Mat img){

        String templatePath = "/storage/emulated/0/MusicRec/Template/template.jpg";
        String templatePath2 = "/storage/emulated/0/MusicRec/Template/template2.jpg";
        String templateChiaveVPath = "/storage/emulated/0/MusicRec/Template/templateChiaveViolino.jpg";
//        String templateWholeRestPath = "/storage/emulated/0/MusicRec/Template/templateWholeRest.jpg";
        String templateSharpPath = "/storage/emulated/0/MusicRec/Template/templateSharp.jpg";

        Mat template  = Highgui.imread(templatePath);
        Mat template2 = Highgui.imread(templatePath2);
        Mat templateSharp = Highgui.imread(templateSharpPath);
//        Mat templateWholeRest = Highgui.imread(templateWholeRestPath);
        Mat templateChiaveViolino = Highgui.imread(templateChiaveVPath);

        detectObjectbyTemplate(template, img, halfnotes, 0.42 + param1/50);
        detectObjectbyTemplate(template2, img, halfnotes, 0.38 + param1/50);
        detectObjectbyTemplate(templateChiaveViolino, img, trebleclef, 0.35 + param1/50);
//        detectObjectbyTemplate(templateWholeRest, img, halfnotes, 0.92);
        detectObjectbyTemplate(templateSharp, img, sharps, 0.4 + param1/50);
    }

    /**
     * This function is used to find object in an image using a template. It call inside the method matchTemplate
     *@param template The template image of the object to find
     *@param  src the image in which the search template
     *@param arrayrect array used to save the position of object detected
     *@param threshold value of threshold used to manage the object recognition
     */
    private void detectObjectbyTemplate(Mat template, Mat src, ArrayList<Rect> arrayrect, double threshold){
        Mat process = new Mat(src.height(), src.width() + template.width(), src.type());
        Mat left = new Mat(process, new Rect(0,0, src.width(), src.height()));
        src.copyTo(left);
        Mat right = new Mat(process, new Rect(src.width(), 0 , template.width(), template.height()));
        template.copyTo(right);

        int match_method = Imgproc.TM_CCOEFF;
        int result_cols = process.cols() - template.cols() + 1;
        int result_rows = process.rows() - template.rows() + 1;
        Mat result = new Mat(result_rows, result_cols, CvType.CV_32FC1);

        Imgproc.matchTemplate(process,template,result, match_method);
        Core.normalize(result, result, 0, 1, Core.NORM_MINMAX, -1, new Mat());
        Imgproc.threshold(result, result, threshold, 1, Imgproc.THRESH_TOZERO);

        while(true) {
            Core.MinMaxLocResult mmr = Core.minMaxLoc(result);
            Point matchLoc;
            if (match_method == Imgproc.TM_SQDIFF || match_method == Imgproc.TM_SQDIFF_NORMED) {
                matchLoc = mmr.minLoc;
            } else {
                matchLoc = mmr.maxLoc;
            }
            if(mmr.maxVal >= threshold) {

                if( matchLoc.x+template.cols()/2 < src.cols()) {
                    Rect roi = new Rect((int) matchLoc.x, (int) matchLoc.y, template.cols(), template.rows());
                    arrayrect.add(roi);
                }
                Imgproc.floodFill(result, new Mat(), mmr.maxLoc, new Scalar(0), new Rect(), new Scalar(.1), new Scalar(1.), 4);
            }
            else{
                break;
            }
        }
    }

}
