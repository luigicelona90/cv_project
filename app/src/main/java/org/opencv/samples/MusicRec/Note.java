package org.opencv.samples.MusicRec;

import org.opencv.core.Point;
import org.opencv.core.Rect;

/**
 * Created by Luigi on 13/03/2015.
 */

/**
 * This class contains all the information about a musical note
 */
public class Note implements  Comparable<Note> {
    private Rect boundingboxNote;
    private Point center;
    private int duration;
    private String height;
    private int numRow;

    public Note ()
    {

    }

    public Rect getBoundingboxNote() {
        return boundingboxNote;
    }

    public void setBoundingboxNote(Rect boundingboxNote) {
        this.boundingboxNote = boundingboxNote;
    }

    public Point getCenter() {
        return center;
    }

    public void setCenter(Point center) {
        this.center = center;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public int getNumRow() {
        return numRow;
    }

    public void setNumRow(int numRow) {
        this.numRow = numRow;
    }


    @Override
    public int compareTo(Note note) {
        return (int)(this.getCenter().x - note.getCenter().x);
    }
}
