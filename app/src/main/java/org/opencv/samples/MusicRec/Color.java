package org.opencv.samples.MusicRec;


import org.opencv.core.Scalar;

/**
 * This class defines some colors, useful for the application
 */


public class Color {
    public static Scalar COLOR_RED = new Scalar(255,0,0);
    public static Scalar COLOR_GREEN = new Scalar(0,255,0);
    public static Scalar COLOR_BLUE = new Scalar(0,0,255);
    public static Scalar COLOR_YELLOW = new Scalar(240,232,64);
    public static Scalar COLOR_VIOLET = new Scalar(143,0,255);
    public static Scalar COLOR_WHITE = new Scalar(255,0,0);
}
