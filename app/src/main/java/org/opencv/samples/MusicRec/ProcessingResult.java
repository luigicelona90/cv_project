package org.opencv.samples.MusicRec;

import org.opencv.core.Mat;
import org.opencv.core.Rect;

import java.util.ArrayList;

/**
 * Created by Luigi on 27/03/2015.
 */

/**
 * This class is used like a container to pass heterogeneous data from an asynctask to the main thread
 */
public class ProcessingResult {
    public ArrayList<Mat> mats;
    public ArrayList<Rect> object ;
    public ArrayList<Note> noteDetected ;
    public Sheet stave ;

    public  ProcessingResult(){
        mats = new ArrayList<Mat>();
        object = new ArrayList<Rect>();
        noteDetected = new ArrayList<Note>();
        stave =  new Sheet();
    }

}
